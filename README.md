# Freelancer Template - README #
---

### Overview ###

**Freelancer** is a fast and responsive theme for the [**Bludit**](https://www.bludit.com/) CMS. The template is made out of plain PHP/HTML/CSS/JS. No jQuery, no Bootstrap, no other external framework, no heavy Google fonts or other bloat was used. Therefore the template is small and really fast to load. There are three columns, the left one shows the news (posts), the right one shows some images of the the latest work and in the middle the content/information of the actual page. There is also a full width portfolio page and a link template for the left column. There are some small configuration files where you can specify some settings and the template parts to use. The code is well documented with comments. The template is responsive and should look great on any device. The used example images are from [**Streetwill**](http://streetwill.co/) and are licensed under [**CC0 - No Rights Reserved**](https://creativecommons.org/share-your-work/public-domain/cc0/).

### Screenshots ###

![Freelancer - Home - Large Desktop](development/readme/freelancer_1.jpg "Freelancer - Home - Large Desktop")  
![Freelancer - Home - Medium Desktop](development/readme/freelancer_2.jpg "Freelancer - Home - Medium Desktop")  
![Freelancer - Home - Small Desktop](development/readme/freelancer_3.jpg "Freelancer - Home - Small Desktop")  
![Freelancer - Post - Small Desktop](development/readme/freelancer_4.jpg "Freelancer - Post - Small Desktop")  
![Freelancer - Portfolio - Small Desktop](development/readme/freelancer_5.jpg "Freelancer - Portfolio - Small Desktop")  
![Freelancer - Home - Mobile](development/readme/freelancer_6.jpg "Freelancer - Home - Mobile")  
![Freelancer - News - Mobile](development/readme/freelancer_7.jpg "Freelancer - News - Mobile")  
![Freelancer - Recent Work - Mobile](development/readme/freelancer_8.jpg "Freelancer - Recent Work - Mobile")  
![Freelancer - Menu - Mobile](development/readme/freelancer_9.jpg "Freelancer - Menu - Mobile")  
![Freelancer - Portfolio - Mobile](development/readme/freelancer_10.jpg "Freelancer - Portfolio - Mobile")  

### Setup ###

* Copy the directory **freelancer** to the theme directory of your Bludit installation.
* Normally this is under **bludit/bl-themes/**.
* Activate the theme in the Bludit theme settings.
* The following files are used for customizing the theme:  
-- Main configuration: **freelancer/php/config/config.php**.  
-- Footer: **freelancer/php/bottom/footer.php**.  
-- Linkbar: **freelancer/php/left/links.php**.  
-- 3 columns page: **freelancer/php/template/default.php**.  
-- Full width page: **freelancer/php/template/default_full.php**.  
-- Homepage: **freelancer/php/template/home.php**.  
-- Portfolio: **freelancer/php/template/portfolio_full.php**.  
* Open the files mentioned above in your favorite text editor.  
* Edit the settings and save/upload the customized files.  
* You can replace the header image **freelancer/img/back.jpg** with your title image.  
* Check the page in your browser and make modifications if necessary.  
* I would like to know if you are using this template, so write me a line to **biegel[at]gmx.ch**!  
* Maybe you could also let the link in the footer :-) But hey, you can do whatever you want! No restrictions.  

### Support ###

This is a free template and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Freelancer** template is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
